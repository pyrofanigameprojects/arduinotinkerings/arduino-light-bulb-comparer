#include <LiquidCrystal.h>
#include <stdlib.h>

//Analog//
const int analogid[4] = { A1, A2, A3, A5 };
const String analog_name[4] = { "A0", "A1", "A2", "A3" };

float analog_value[4] = { 0.0, 0.0, 0.0, 0.0 };

String sorted_list[4] = { "", "", "", "" };

//Analog//
//Sorting
float sorted_analog_value[4] = { 0.0, 0.0, 0.0, 0.0 };

float analog_snapshot_list[4] = { 0.0, 0.0, 0.0, 0.0 };
float analog_snapshot_list_index_holder[4] = { 0.0, 0.0, 0.0, 0.0 };

int loop_circles = 0;
int loop_circles_sorting = 0;

//

//Screen
enum keys { none,
            select,
            left,
            down,
            up,
            right
};

const int button_id = A0;
String currentKeyName = "";
keys currentKey;

int workSpaceINT = 0;
int screenButtonsPinAvrg = 0;

const String displayUpString = " ";
const String displayDownStrings[4] = { "  ", "  ", " ", "  " };


LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

//
//Relays
bool light_relays[4] = { false, false, false, false };
const int relayid[4] = { 3, 11, 12, 13 };
float timer = 0.0;
//



void Inititalization() {

  initialize_analog();
  initialize_relaus();
  initialize_sort_list();
  initialize_screen();
}
void initialize_analog() {
  //analog
  Serial.begin(9600);
  //
}
void initialize_relaus() {
  //Relays//
  for (int i = 0; i < 4; i++) {
    pinMode(relayid[i], OUTPUT);
  }
  //
}
void initialize_sort_list() {
  //sort list
  for (int i = 0; i < 4; i++) {
    sorted_list[i] = "AN";
  }
}
void initialize_screen() {
  //screen
  lcd.begin(16, 2);
  SetStrings(1);
  PrintStrings(1);
  workSpaceINT = 0;
}
void setup() {
  // put your setup code here, to run once:
  Inititalization();
}

void loop() {
  // put your main code here, to run repeatedly:
  relay_loop();
  if (RelaysStates()) {
    analog_values_loop();
    sort_loop();
  }
  Screen_loop();
}

void analog_values_loop() {
  loop_circles++;
  if (loop_circles > 40) {
    ReadAnalogueSensors();
    loop_circles = 0;
  }
}
void sort_loop() {
  loop_circles_sorting++;
  if (loop_circles_sorting > 20) {
    sortITNow();
    loop_circles_sorting = 0;
  }
}

void Screen_loop() {
  GetKey();
  SetAndPrintConstantUpdatedValue();
}

void relay_loop() {
  ManageRelays(workSpaceINT);
  DisableOnTimeRelays();

  TimerCount();
}

void ReadAnalogueSensors() {

  for (int i = 0; i < 4; i++) {
    analog_value[i] = analogRead(analogid[i]);
  }
}


void GetKey() {
  screenButtonsPinAvrg = analogRead(button_id);

  if (screenButtonsPinAvrg >= 818)  ///nothing from4 volts and up
  {
    currentKey = none;
    currentKeyName = "none";
  }
  if (630 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 800) {
    currentKey = select;
    currentKeyName = "select";
    resetTimer();

    currentKey = none;
  }
  if (400 <= screenButtonsPinAvrg && 600 > screenButtonsPinAvrg) {
    currentKey = left;
    currentKeyName = "left";
    workSpacesIndex();
    SetStrings(workSpaceINT);
    PrintStrings(workSpaceINT);
    currentKey = none;
  }
  if (204 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 300) {
    currentKey = down;
    currentKeyName = "down";
    SetRelays(workSpaceINT);
    currentKey = none;
  }
  if (90 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 150) {
    currentKey = up;
    currentKeyName = "up";
    SetRelays(workSpaceINT);
    currentKey = none;
  }
  if (screenButtonsPinAvrg < 61) {
    currentKey = right;
    currentKeyName = "right";
    workSpacesIndex();
    SetStrings(workSpaceINT);
    PrintStrings(workSpaceINT);
    currentKey = none;
  }
  delay(150);
}
void workSpacesIndex() {
  if (currentKey == left && workSpaceINT >= 0) {
    workSpaceINT--;
  }
  if (currentKey == right && workSpaceINT < 4) {
    workSpaceINT++;
  }
  if (workSpaceINT < 0) {
    workSpaceINT = 3;
  }
  if (workSpaceINT > 3) {
    workSpaceINT = 0;
  }
}

void SetStrings(int workspace_index) {
  displayDownStrings[workspace_index] = "A" + String(workspace_index) + ":" + String(analog_value[workspace_index], 1);
}


void PrintStrings(int index) {
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print(displayDownStrings[index]);
}
void SetAndPrintConstantUpdatedValue() {
  displayUpString = sorted_list[0] + ">" + sorted_list[1] + ">" + sorted_list[2] + ">" + sorted_list[3];
  lcd.setCursor(0, 0);
  lcd.print(displayUpString);
  lcd.setCursor(11, 1);
  lcd.print(timer_string());
}
String timer_string() {
  return "T:" + String(timer / 720, 1);
}
///Relays
void SetRelays(int workspaceINDX) {
  if (currentKey == up) {
    light_relays[workspaceINDX] = true;
  }
  if (currentKey == down) {
    light_relays[workspaceINDX] = false;
  }
}

void TimerCount() {
  if (RelaysStates()) {
    timer += 1.0;
  } else {
    timer = 0.0;
  }
}
void DisableOnTimeRelays() {
  if (timer >= 720 && RelaysStates()) {
    for (int i = 0; i < 4; i++) {
      light_relays[i] = false;
      ManageRelays(i);
    }
  }
}
void resetTimer() {
  if (currentKey == select) {

    timer = 0.0;
  }
}
bool RelaysStates() {

  return light_relays[0] || light_relays[1] || light_relays[2] || light_relays[3];
}
void ActivateRelays(int currentRelay, bool activate) {
  if (!activate) {
    digitalWrite(currentRelay, HIGH);
  } else {
    digitalWrite(currentRelay, LOW);
  }
}

void ManageRelays(int i) {


  ActivateRelays(relayid[i], light_relays[i]);
}
///Relays

//Sort Values

int compare(const void* a, const void* b) {
  return (*(int*)a - *(int*)b);
}


void sortITNow() {

  take_snapshot();
  qsort(analog_snapshot_list, 4, sizeof(analog_snapshot_list[0]), compare);  //sorts
  matchin_stuff();
}


void matchin_stuff() {
  for (int i = 0; i < 4; i++) {
    for (int y = 0; y < 4; y++) {
      if (matched(analog_snapshot_list[i], analog_snapshot_list_index_holder[y])) {
        sorted_list[i] = analog_name[y];
      }
    }
  }
}
void take_snapshot() {
  for (int i = 0; i < 4; i++) {
    analog_snapshot_list[i] = analog_value[i];
    analog_snapshot_list_index_holder[i] = analog_snapshot_list[i];
  }
}

bool matched(float value, float real_value) {
  return value == real_value;
  //return value - value * 0.2 <= real_value && real_value <= value + value * 0.2;
}

String matched_string(int index) {
  return analog_name[index];
}